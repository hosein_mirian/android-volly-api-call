package mirian.vollyreceiveproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    // Todo : Will show the string "data" that holds the results
    TextView results;
    // Todo :  URL of object to be parsed
    String JsonURL = "https://www.vegguide.org/region/370";

    // Todo : Defining the Volley request queue that handles the URL request concurrently
    RequestQueue requestQueue;

    Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Todo :  Creates the Volley request queue

        requestQueue = Volley.newRequestQueue(this);

        // Todo : Casts results into the TextView And Button found within the main layout XML with id jsonData

        results = findViewById(R.id.jsonData);
        btn = findViewById(R.id.btn_json);

        /*Todo : Set OnClick To Get JSON Data In TextView*/

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Todo : Creating the JsonObjectRequest class called obreq, passing required parameters:
                // Todo : GET is used to fetch data from the server, JsonURL is the URL to be fetched from.


                JsonObjectRequest jsObjRequest = new JsonObjectRequest
                        (Request.Method.GET, JsonURL, null,

                                new Response.Listener<JSONObject>() {

                                    @Override
                                    public void onResponse(JSONObject response) {

                                        /*Todo : Assign Json Value In TextView*/
                                        try {
                                            Log.i("Hosein", response.toString());
                                            JSONObject mainObject = new JSONObject(response.toString());
                                            JSONObject parent = mainObject.getJSONObject("parent");
                                            JSONArray comments = mainObject.getJSONArray("comments");
                                            int is_country = parent.getInt("is_country");

                                            results.setText("JSON Data Is \n " + is_country);
//                                          results.setText("JSON Data Is \n: " + comments);

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                results.setText("JSON Data Is \n: " + error.toString());

                            }

                        }) {
                    /** Passing some request headers* */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/json; charset=UTF-8");
                        params.put("Authorization", "application/vnd.vegguide.org-entries+json");
                        //params.put("token", ACCESS_TOKEN);
                        return params;
                    }
                };

                // Todo : Adds the JSON object request "jsObjRequest" to the request queue

                requestQueue.add(jsObjRequest);
            }
        });


    }

}


